// console.log("Hello World");

// Assignment operator (=)
// Tha assignment operator adds the value of the right operand to a variable and assigns the result to the variable

let assignmentNumber = 8;
console.log(assignmentNumber);


// Addition Assignment Operator

let totalNumber = assignmentNumber + 2;
console.log ("Result of addition assignment operator: " + totalNumber);

// Arithmetic Operator ("+", "-", "*", "/", "%")

let x = 2;
let y = 5;


let sum = x + y;
console.log ("Result of Addition Operator: " + sum);

let difference = x - y ;
console.log ("Result of Subtraction Operator: " + difference);

let product =  x * y ;
console.log ("Result of Multiplication Operator: " + product);

let quotient =  y / x ;
console.log ("Result of Quotient Operator: " + quotient);

let remainder = y % x ; // remainder lang kukuhain
console.log ("Result of Modulo Operator: " +remainder);



// Mini Activity


// Multiple Operators and Parenthesis (MDAS and PMDAS)

/*- When multiple operators are applied in a single statement, it follows the PEMDAS (Parenthesis, Exponents, Multiplication, Division, Addition and Subtraction) rule
            - The operations were done in the following order:
                1. 3 * 4 = 12
                2. 12 / 5 = 2.4
                3. 1 + 2 = 3
                4. 3 - 2.4 = 0.6*/



let mDas = 1 + 2 -3 * 4 / 5;
console.log ("Result of MDAS operator: " + mDas);

/*let mDas2 = 6 / 2 * (1 + 2);


	
		1. (1 + 2) = 3
		2. (2 * 3) = 6
		3. (6 / 6) = 
	*/
/*console.log ("Result of MDAS operator: " +mDas2);
*/



let pMdas = 1 + (2 - 3) * (4 / 5);
/*
	1. 4 / 5 = 0.8
	2. 2 - 3 = -1
	3. -1 * 0.8 = -0.8
	4. 1 + -0.8 = 0.2 / 0.199 


*/


console.log (pMdas);


// Comparison Operator
// Equality Operator ("==" )

/*
	- check whether the operands are equal / have the sames content.
	Note : "=" is for assignment operator
	Note : "==" is for equality operator

	- Attempts to convert and compared operands of different data types.

	True == 1 and false == 0
*/

let juan = 'juan';
	console.log(1 == 1); // true
	console.log(1 == 2);// false
	console.log(1 == '1');// true
	console.log(0 == false);//true
	//Compares two strings that are the same 
	console.log('juan' == 'juan'); // true
	//Compares a string with the variable "juan" declared above 
	console.log('juan' == juan);// true


// Inequality operator

/* 
            - Checks whether the operands are not equal/have different content
            - Attempts to CONVERT AND COMPARE operands of different data types
        */


		console.log(1 != 1);//false
        console.log(1 != 2);//true
        console.log(1 != '1');//false
        console.log(0 != false);//false
        console.log('juan' != 'juan');//false
        console.log('juan' != juan);//false


        // Strict Equality Operator

        // (===)

        /* 
            - Checks whether the operands are equal/have the same content
            - Also COMPARES the data types of 2 values
            - JavaScript is a loosely typed language meaning that values of different data types can be stored in variables
            - In combination with type coercion, this sometimes creates problems within our code (e.g. Java, Typescript)
            - Some programming languages require the developers to explicitly define the data type stored in variables to prevent this from happening
            - Strict equality operators are better to use in most cases to ensure that data types provided are correct
        */


        console.log(1 === 1); // true
        console.log(1 === 2);//false
        console.log(1 === '1');//false
        console.log(0 === false);//false
        console.log('juan' === 'juan');//true
        console.log('juan' === juan);//true


       // Relational Operators


      //Some comparison operators check whether one value is greater or less than to the other value.

        let a = 50;
        let b = 65;

        //GT or Greater Than operator ( > )
        let isGreaterThan = a > b; // 50 > 65 // false
        //LT or Less Than operator ( < )
        let isLessThan = a < b; // 50 < 65
        //GTE or Greater Than Or Equal operator ( >= ) 
        let isGTorEqual = a >= b; // 50 >= 65
        //LTE or Less Than Or Equal operator ( <= ) 
        let isLTorEqual = a <= b; // 50 <= 65

        //Like our equality comparison operators, relational operators also return boolean which we can save in a variable or use in a conditional statement.

        console.log(isGreaterThan);
        console.log(isLessThan);
        console.log(isGTorEqual);
        console.log(isLTorEqual);


        // Logical Operators (&&, ||, !)

        let isLegalAge = true;
        let isRegistered = false;

        // Logical Operator (&& - double ampersand or And operator)
        // Return true if all operands are true

        /*
			&& = And   || = Or   ! = Not

			1 && 1 =true
			1 && 0 = fasle
			0 && 1 =false
			0 && 0 = false


			Example : 
			 let verification = email && password;

	
        */



        let allRequirementsMet = isLegalAge && isRegistered;
        console.log("Result of logical and operator: " + allRequirementsMet);



// Logical Or Operator (|| - Double Pipe)
	// Returns true if one of the operands are true 

	// 1 || 1 = true
	// 1 || 0 = true
	// 0 || 1 = true
	// 0 || 0 = false

	let someRequirementsMet = isLegalAge || isRegistered; 
	console.log("Result of logical OR Operator: " + someRequirementsMet);

       


       /// Logical Not Operator (! - Exclamation Point)
        // Returns the opposite value 
        let someRequirementsNotMet = !isRegistered;
        console.log("Result of logical NOT Operator: " + someRequirementsNotMet);


        let Requirements = !isLegalAge || !isRegistered;
        console.log(Requirements);